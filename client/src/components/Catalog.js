import React from 'react';

const Catalog = ({products, handleAddToCart}) => {
	
	return (

		<div className="container">
			<h1>Products</h1>
			<div className="row">
				
					{
						products.map( product => {
							return (
								<div className="col-12 col-md-3">
								<div className="card">
									<div className="card-header">
										<img src={"http://localhost:3001" + product.image} className="card-img-top"/>
									</div>
									<div className="card-body">
										<p className="card-item">{product.name}</p>
										<p className="card-item">{product.categoryId}</p>
										<p className="card-item">{product.price}</p>
										<p className="card-item">{product.description}</p>
										<button onClick={()=>{handleAddToCart(product)}} className="btn btn-dark" type="submit">Add to Cart</button>
									</div>

								</div>
								</div>
							)
						})
					}
				
			</div>
		</div>
	)
}

export default Catalog;