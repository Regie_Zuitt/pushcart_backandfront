import React from 'react';

//forms
import AddProduct from './forms/AddProduct';
import AddCategory from './forms/AddCategory';
import EditProduct from './forms/EditProduct';
import EditCategory from './forms/EditCategory';
import DeleteProduct from './forms/DeleteProduct';
import DeleteCategory from './forms/DeleteCategory';


const AdminPanel = ({categories, handleChangeCategoriesStatus, categoriesStatus, products}) => {
	return(
		<div className="container mt-5">
			<div className="row">
				<div className="col-12 col-md-7 ">
					<AddProduct categories={categories} />
					<EditProduct  
						categories={categories}
						products={products}
					/>	
					<DeleteProduct 
						products={products}
					/>
				</div>

				<div className="col-12 col-md-5">
					<div className="row sticky-top">
						<div className="col">
							<AddCategory />
							<EditCategory 
								categories={categories} 
								handleChangeCategoriesStatus={handleChangeCategoriesStatus}
								categoriesStatus={categoriesStatus}
							/>
							<DeleteCategory categories={categories} />
						</div>
					</div>											
				</div>
			</div>
		</div>
	);
}

export default AdminPanel;