import React, { useState } from 'react';
import TransactionDetails from './forms/TransactionDetails';
import TransactionItem from './forms/TransactionItem';

const Transaction = () => {



	return(
		<React.Fragment>
			<div className="container">
				<div className="row">
					<div className="col-12 col-md-9 py-3 sticky-top">
						<TransactionDetails />
					</div>
					<div className="col-12 col-md-9 py-3">
						<div className="row">
							<div className="col-12">
								<TransactionItem />
								<TransactionItem />
								<TransactionItem />
								<TransactionItem />
								<TransactionItem />
							</div>
						</div>
					</div>
				</div> 
			</div>			
		</React.Fragment>
		
		
	)
}

export default Transaction;