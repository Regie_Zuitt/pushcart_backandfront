import React, { useState } from 'react';

const EditCategory = ({categories, handleChangeCategoriesStatus, categoriesStatus}) => {

	const [selectedCategory, setSelectedCategory] = useState({
		name: null,
		id: null
	})

	const handleChangeSelected = (e) => {
		let categorySelected = categories.find( category => {
				return category._id == e.target.value
		})
		setSelectedCategory({
			id: e.target.value,
			name: categorySelected.name
		})
	}

	const handleChangeName = e => {
		setSelectedCategory({
			...selectedCategory,
			[e.target.name] : e.target.value
		})
	}

	const handleEditCategory = e => {
		e.preventDefault();

		fetch("http://localhost:3001/categories/"+selectedCategory.id,{
			method: "PUT",
			body: JSON.stringify({ name : selectedCategory.name }),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem('token')
			}
		})
		.then( data => {
				
			return data.json()})
		.then( result => {
			handleChangeCategoriesStatus({
				lastUpdated : selectedCategory.id,
				status: "pass",
				isLoading: true
			})
		})
	}

	return(
		<div className="mb-5 bg-secondary">
			<h2 className="text-center">Edit Category</h2>

			<form action="" onSubmit={handleEditCategory}>

				{
					categoriesStatus.isLoading ?
					<div class="spinner-grow" role="status">
					  <span class="sr-only">Loading...</span>
					</div>
					:
					<React.Fragment>
						<select name="category" id="category" className="form-control mb-2" onChange={handleChangeSelected}>
									<option disabled selected>Select Category</option>
							{ categories.map(category => {
								return (
									<option value={category._id}>{category.name}</option>
								)
							})
							}
						</select>

						<input 
							type="text" 
							name="name" 
							id="name" 
							placeholder="Category Name" 
							className="form-control mb-2" 
							value={selectedCategory.name} 
							onChange={handleChangeName}
							/>

						<button className="btn btn-primary">Save Changes</button>
					</React.Fragment>
				}
				
			</form>
			
		</div>
	);
}

export default EditCategory;