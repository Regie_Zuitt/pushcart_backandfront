import React, { useState } from 'react';

const AddProduct = ({categories}) => {

	const [product, setProduct] = useState({
		name: null,
		description: null,
		price: null,
		categoryId: null,
		image: null

	})

	const onChangeText = e => {
		setProduct({
			...product,
			[e.target.name] : e.target.value
		})
	}


	const handleChangeFile = e => {
		
			setProduct({
				...product,
				image: e.target.files[0]
		})
	}

	const handleAddProduct = e => {
		e.preventDefault();
		const formData = new FormData();

		formData.append('name', product.name)
		formData.append('price', product.price)
		formData.append('categoryId', product.categoryId)
		formData.append('description', product.description)
		formData.append('image', product.image)

		let url = "http://localhost:3001/products"

		fetch(url,{
			method: "POST",
			body: formData,
			headers :{
				"Authorization": localStorage.getItem('token')
			}
		})
		.then( data => data.json())
		.then( result => {
			console.log(result)
		})
	}


	

	return(
		<div className="bg-secondary text-light mb-5">
			<h2 className="text-center">Add Product page</h2>

			<form action="" enctype="multipart/form-data" onSubmit={handleAddProduct}>
				<input 
					type="text" 
					name="name" 
					id="name" 
					placeholder="Product Name" 
					className="form-control mb-2" 
					onChange={onChangeText}
				/>
				<input 
					type="text" 
					name="price" 
					id="price" 
					placeholder="Product Price" 
					className="form-control mb-2" 
					onChange={onChangeText}
				/>
				<select name="categoryId" id="categoryId" className="form-control mb-2" onChange={onChangeText}>
					<option disabled selected>Select Category</option>
					{
						categories.map( category => {
							return (
								<option value={category._id}>{category.name}</option>
							)
						})
					}
					
				</select>

				<input type="file" name="image" 
					id="image" className="form-control-file mb-2" 
					placeholder="Product Image" onChange={handleChangeFile} />

				<textarea 
					name="description" 
					id="description" 
					col="30" rows="10" 
					className="form-control mb-2" 
					placeholder="Product Description"
					onChange={onChangeText}
				></textarea>

				<button className="btn btn-primary mb-2">Create Product</button>
			</form>
		</div>
	);
}

export default AddProduct;