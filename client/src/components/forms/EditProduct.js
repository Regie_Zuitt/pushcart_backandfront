import React, { useState } from 'react';

const EditProduct = ({categories, products}) => {

	const [selectedProduct, setSelectedProduct] = useState({
		name: null,
		price: null,
		categoryId: null,
		image: null,
		description: null,
		id: null,
		categoryName: null
	})

	const handleSelectedProduct = (e) => {
		let selected = products.find( product => {
				return product._id == e.target.value
		})
		console.log(selected)

		let currentCategory = categories.find( category => {
			console.log(selected.categoryId)
			return category._id === selected.categoryId
		})

		setSelectedProduct({
			...selected,
			image: "http://localhost:3001"+selected.image,
			categoryName : currentCategory.name
		})
	}

	const handleChange = e => {
		setSelectedProduct({
			...selectedProduct,
			[e.target.name] : e.target.value
		})
		console.log(typeof selectedProduct.image)
	}

	const handleChangeFile = e => {
		setSelectedProduct({
			...selectedProduct,
			image : e.target.files[0]
		})

		console.log(selectedProduct)
	}

	const handleEditProduct = e => {
		e.preventDefault();

		let formData = new FormData();
		formData.append('name', selectedProduct.name)
		formData.append('price', selectedProduct.price)
		formData.append('categoryId', selectedProduct.categoryId)
		formData.append('description', selectedProduct.description)

		if (typeof selectedProduct.image === 'object') {
			formData.append('image', selectedProduct.image)
		}

		let url = "http://localhost:3001/products/"+selectedProduct._id;

		fetch(url, {
			method : "PUT",
			headers: {
				"Authorization" : localStorage.getItem('token')
			},
			body : formData
		})
		.then( data => data.json())
		.then( result => console.log(result))
	}

	return(
		<div className="bg-secondary text-light mb-5">
			<h2 className="text-center">Edit Product page</h2>

			<select name="product" id="product" className="form-control mb-2" onChange={handleSelectedProduct}>
					<option selected disabled>Select Product</option>
					{products.map(product => {
						return(
							<option value={product._id}>{product.name}</option>
						)
					})
					}
					
			</select>
			<div>
				<div className="row">
					<div className="col-4">
						<img src={selectedProduct.image} alt="" height="120px" width="120px"className="ml-2"/>
					</div>
					<div className="col-8">
						<p className="mb-0">Name: {selectedProduct.name}</p>
						<p className="mb-0">Price: {selectedProduct.price}</p>
						<p className="mb-0">Description: {selectedProduct.description}</p>
						<p className="mb-0">Category: {selectedProduct.categoryName}</p>
					</div>
				</div>
			</div>


			<form action="" onSubmit={handleEditProduct}>
				<div>
					<img />
				</div>
				<input type="text" name="name" 
					id="name" placeholder="Product Name" 
					className="form-control mb-2" value={selectedProduct.name}
					onChange={handleChange}
				/>
				<input type="text" name="price" 
					id="price" placeholder="Product Price" 
					className="form-control mb-2" value={selectedProduct.price}
					onChange={handleChange}
				/>
				<select name="categoryId" id="categoryId" 
					className="form-control mb-2"
					onChange={handleChange}
				>
					
					{categories.map(category => {
						console.log(category._id)
						return(
							
							category._id === selectedProduct.categoryId ?
							<option value={category._id}> {category.name} </option>
							: 
							<option value={category._id} selected> {category.name}</option>
						)
					})}
					
				</select>

				<input type="file" name="image" 
					id="image" className="form-control-file mb-2" 
					placeholder="Product Image" 
					onChange={handleChangeFile}
				/>

				<textarea name="description" id="description" 
					col="30" rows="10" className="form-control mb-2" 
					placeholder="Product Description"
					value={selectedProduct.description}
					onChange={handleChange}
				></textarea>

				<button className="btn btn-primary mb-2">Edit Product</button>
			</form>
		</div>
	);
}

export default EditProduct;