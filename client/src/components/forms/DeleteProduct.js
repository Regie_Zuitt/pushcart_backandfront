import React, { useState } from 'react';

const DeleteProduct = ({products}) => {

	const [selectedProduct, setSelectedProduct] = useState({
		_id: null
	})

	const handleChange = e => {
		setSelectedProduct({
			_id : e.target.value
		})
	}

	const handleDeleteProduct = (e) => {
		e.preventDefault()

		fetch('http://localhost:3001/products/'+selectedProduct._id, {
			method: "Delete",
			headers: {
				"Authorization" : localStorage.getItem('token')
			}
		})
		.then( data => data.json())
		.then( res => console.log(res))
	}

	return(
		<React.Fragment>
		
				<h2>Delete Product page</h2>

				<form action="" onSubmit={handleDeleteProduct}>
					<select name="productId" id="product" className="form-control" onChange={handleChange}>
						<option disabled selected>Select Product</option>
						{products.map(product => {
							return(
								<option value={product._id}>{product.name}</option>
							)
						})}
					</select>
					<button className="btn btn-danger my-2">Delete Product</button>
				</form>
			
		</React.Fragment>
	);
}

export default DeleteProduct;