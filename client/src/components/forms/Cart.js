import React, { useState } from 'react';
// import TransactionForm from './forms/TransactionForm';

const Cart = ({cart, handleClearCart, handleRemoveItem, total}) => {

	const handleCheckout = () => {
		console.log("handleCheckout")
		let orders = cart.map(item => {
			return {
				id: item._id,
				qty: 1,
			}
		})
		// console.log({orders : orders})

		fetch("http://localhost:3001/transactions", {
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem('token')
			},
			body : JSON.stringify({orders})
		})
		.then(data => data.json())
		.then( result => console.log(result))
	}

	return(
		<div className="container my-5">
			<div className="row">
				<div className="col-12 col-md-12">
				{
					cart.length ?
					<React.Fragment> 
						<table className="table table-striped table-dark text-center">
							  <thead>
							    <tr>
							   
							      <th scope="col">Product</th>
							      <th scope="col">Price</th>
							      <th scope="col">Action</th>
							    </tr>
							  </thead>
							  <tbody>

							 {
							 	
							 	cart.map( item => {
							 		
							 		return(
						 				<tr>
						 				  
						 				  <td>Mark</td>
						 				  <td>Otto</td>
						 				  <td>
						 				  	<button onClick={()=>{handleRemoveItem(item)}} className="btn btn-danger">Remove from cart</button>
						 				  </td>
						 				</tr>
							 		)
							 	})
							 }
							    
							  </tbody>
						  <tfoot>
						  	<tr>
						  		<td className="text-right">Total</td>
						  		<td>{total}</td>
						  		<td>
						  			<button onClick={handleCheckout} className="btn btn-primary">Checkout</button>
						  		</td>
						  	</tr>
						  </tfoot>
						</table>
						<button className="btn btn-warning" onClick={handleClearCart}>Clear Cart</button>
					</React.Fragment>

					:

					<h3>Cart is empty!</h3>
				}
					
					
					
				</div>
			</div>
		</div>
		
	)
}

export default Cart;