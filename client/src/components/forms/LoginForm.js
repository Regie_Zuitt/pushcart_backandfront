import React, { useState } from 'react';

const LoginForm = () => {

	const [formData, setFormdata] = useState({
		email : null,
		password :null
	})

	const [result, setResult] = useState({
		message: null,
		successful : null
	})

	const handleChange = e => {
		setFormdata({
			...formData,
			[e.target.name] : e.target.value
		})
	}

	const handleLogin = e => {
		e.preventDefault();
		let data = {
			email: formData.email,
			password: formData.password,
		}

		fetch('http://localhost:3001/users/login', {
			method : "POST",
			body : JSON.stringify(data),
			headers : {
				"Content-Type" : "application/json"
			}
		})
		.then( data => data.json() )
		.then( user => {
			console.log(user)

			if (user.token) {
				setResult({
					successful : true,
					message : user.message
				})
				localStorage.setItem('user', JSON.stringify(user.user));
				localStorage.setItem('token', "Bearer " + user.token);
			} else {
				setResult({
					successful : false,
					message : user.message
				})
			}
		} )
	}

	const resultMessage = () => {

		let classList;
		if (result.successful === true) {
			classList = 'alert alert-success'
		} else {
			classList = 'alert alert-danger'
		}

		return (
			<div className={classList}>
				{result.message}
			</div>
		)
	}

	return(
		<div>
			<h2>Login Form page</h2>
			{result.successful == null ? "" : resultMessage()}
			<form action="" onSubmit={handleLogin}>
				<input 
					type="email" 
					name="email" 
					id="email"
					placeholder="Enter email" 
					className="form-control mb-2"
					onChange={handleChange}
					/>

				<input 
					type="password" 
					name="password" 
					id="password"
					placeholder="Enter password" 
					className="form-control mb-2"
					onChange={handleChange}
					/>

				<button className="btn btn-primary mb-2" type="submit">Login</button>
			</form>
		</div>
	);
}

export default LoginForm;