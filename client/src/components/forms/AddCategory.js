import React, { useState } from 'react';

const AddCategory = () => {

	const [formData, setFormData] = useState("");

	const handleCategoryNameChange = (e) => {
		setFormData(e.target.value)
	}

	const handleAddCategory = (e) => {
		e.preventDefault()
		
		let url = "http://localhost:3001/categories/";
		let data = { name : formData};
		let token = localStorage.getItem('token');


		fetch(url, {
			method : "POST",
			body : JSON.stringify(data),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : token
			}
		})
		.then( data => data.json())
		.then( category => console.log(category) )
	}

	return(
		<div className="mb-5 bg-secondary">
			<h2 className="text-center">Add Category</h2>

			<form action="" onSubmit={handleAddCategory}>
				<input 
					type="text" 
					name="name" 
					id="name"
					placeholder="Category Name" 
					className="form-control mb-2"
					onChange={handleCategoryNameChange}
					/>

				<button className="btn btn-primary mb-2" type="submit">Add Category</button>
			</form>
			
		</div>
	);
}

export default AddCategory;