import React, { useState } from 'react';

const DeleteCategory = ({categories}) => {

	const [selectedCategory, setSelectedCategory] = useState({
		name: null,
		id: null
	})

	const handleChangeSelected = (e) => {
		let categorySelected = categories.find( category => {
			if (category._id === e.target.value) {
				return category.name
			}
		})
		setSelectedCategory({
			id: e.target.value,
			name: categorySelected.name
		})
	}

	const handleDeleteCategory = e => {
		e.preventDefault();

		fetch("http://localhost:3001/categories/"+selectedCategory.id,{
			method: "DELETE",
			body: JSON.stringify({ name : selectedCategory.name }),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem('token')
			}
		})
		.then( data => data.json())
		.then( result => console.log(result))
	}

	return(
		<div className="bg-secondary">
			<h2 className="text-center mb-2">Delete Category page</h2>

			<form action="" onSubmit={handleDeleteCategory}>
				
			
				<select 
					name="category" 
					id="category" 
					className="form-control mb-2"
					onChange={handleChangeSelected}
				>
					<option disabled selected>Select Category</option>
					{categories.map(category => (
					<option value={category._id}>{category.name}</option>
						))}
				</select>

				<button className="btn btn-danger">Delete Category</button>
			</form>
		</div>
	);
}

export default DeleteCategory;