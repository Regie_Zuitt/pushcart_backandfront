import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from './components/layouts/Navbar';
import Catalog from './components/Catalog';

//Forms
import AdminPanel from './components/AdminPanel';
import RegisterForm from './components/forms/RegisterForm';
import LoginForm from './components/forms/LoginForm';
import Cart from './components/forms/Cart';
import Transaction from './components/Transaction';

function App() {

  //states
  const [products, setProducts] = useState([])
  const [categories, setCategories] = useState([])
  const [total, setTotal] = useState(0)
  const [cart, setCart] = useState([])
  const [categoriesStatus, setCategoriesStatus] = useState({
    lastUpdated: null,
    status: null,
    isLoading: true
  })

  useEffect(()=>{
   
    let total =0;
    cart.forEach(item =>{
      total += item.price
    })

    console.log(total)
    setTotal(total)
  },[cart])

  useEffect(() =>{
    fetch("http://localhost:3001/categories")
    .then(res => res.json())
    .then(data => {
      setCategories([...data])
      setCategoriesStatus({isLoading : false})
    })
  },[categoriesStatus.isLoading])

  useEffect( () => {
    fetch("http://localhost:3001/products")
    .then(res => res.json())
    .then(data => {
      setProducts([...data])
    })

    fetch("http://localhost:3001/categories")
    .then(res => res.json())
    .then(data => {
      setCategories([...data])
    })


  },[]);

    // change state methods
    const handleChangeCategoriesStatus = status => {
      setCategoriesStatus(status)
    }

  // cart methods
  const handleAddToCart = (product) => {
    let matched = cart.find(item => {
      return item._id === product._id;
    })
    console.log(matched)
    if(!matched) {

    setCart([
      ...cart,
      product
    ])
    }
  }

  const handleClearCart = () => {
    setCart([])
  }

  const handleRemoveItem = (itemTobeRemove) => {
    let updatedCart = cart.filter( item => item !== itemTobeRemove)
    setCart(updatedCart);
  }


  return (
    <Router>
      <div>
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Catalog 
              products={products} 
              handleAddToCart={handleAddToCart}
            />
          </Route>
          <Route path="/transactions">
            <Transaction />
          </Route>
          <Route path="/adminPanel">
            <AdminPanel             
              categories={categories} 
              handleChangeCategoriesStatus={handleChangeCategoriesStatus}
              categoriesStatus={categoriesStatus}
              products={products}
            />
          </Route>
          <Route path="/cart">
            <Cart 
              cart={cart}
              handleClearCart={handleClearCart}
              handleRemoveItem={handleRemoveItem}
              total={total}
            />
          </Route>
          <Route path="/register">
            <RegisterForm />
          </Route>
          <Route path="/login">
            <LoginForm />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
