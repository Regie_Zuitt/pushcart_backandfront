const router = require('express').Router();
const User = require('../models/Users');
const bcrypt = require('bcrypt');
const passport = require('passport');
require('./../passport-setup');
const jwt = require('jsonwebtoken');




router.post('/register', (req,res,next) =>{
	// res.send("This is register endpoint")

	// 1st get the details from the form
	// 2nd check the completeness mof the form
	// check if password is not less than 8 characters
	// check if password and confirm password matched

	// 1st get the details from the form
	let firstname = req.body.firstname
	let lastname = req.body.lastname
	let email = req.body.email
	let password = req.body.password
	let confirmPassword = req.body.confirmPassword

	// 2nd check the completeness mof the form
	if (!firstname || !lastname || !email || !password || !confirmPassword) {
		return res.status(400).send({
			message: "Incomplete fields"
		})
	}

	// check if password is not less than 8 characters
	if(password.length < 8) {
		return res.status(400).send({
			message : "Password is too short"
		})
	}

	// check if password and confirm password matched
	if (password !== confirmPassword) {
		return res.status(400).send({
			message : "Password do not match"
		})
	}

	// check if email is already in use
	User.findOne({ email : email})
	.then( user => {
		if(user) {
			return res.status(400).send({
				message : "Email is already in use"
			})
		} else {

			const saltRounds = 10;
			bcrypt.genSalt(saltRounds, function(err, salt){
				bcrypt.hash(password, salt, function (err, hash){
					User.create({
						// firstname : firstname,
						// lastname : lastname,
						// password : password,
						// email : email

						// shortcut
						firstname,
						lastname,
						password : hash,
						email
					})
					.then( user => {
						return res.send({user, successMessage : "Account registered"})
					})

				})
			})

			
		}
	});
})
router.post('/profile', passport.authenticate('jwt', {session : false}), 
	function (req,res){
		res.send(req.user)
	})
router.post('/login',(req,res,next) =>{
	let email = req.body.email;
	let password = req.body.password;

	// check if there are credentials
	if (!email || !password) {
		return res.status(400).send({
			message : "Something went wrong"
		})
	}

	// check if it is registered
	User.findOne({email})
	.then( user => {
		// if there are no email matched
		if (!user) {
			return res.status(400).send({
				message : "Something went wrong"
			})
		} else {
			// check if matched password with email holder
			bcrypt.compare(password, user.password, (err, passwordMatch) => {
				if (passwordMatch) {
					let token = jwt.sign({id : user._id}, 'secret');

					return res.send({message : "login successful",
						token : token,
						user : {
							firstname : user.firstname,
							lastname : user.lastname,
							role : user.role,
							id : user._id
						}
					});
				} else {
					return res.send({message : "Something went wrong"})
				}
			})
		}
	})
})

module.exports = router;