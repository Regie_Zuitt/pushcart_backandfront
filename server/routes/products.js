const router = require('express').Router();
const ProductModel = require('../models/Products');
const multer = require('multer');
const passport = require('passport');

function isAdmin(req,res,next){
	if (req.user.role !== 'admin') {
		return res.send({message : "Unauthorized request"})
	} else {
		next()
	}
}

const storage = multer.diskStorage({
	destination : function(req,file,cb){
		cb(null, "public/products")
	},
	filename : function(req,file,cb){
		cb(null, Date.now() + "-" +file.originalname)
	}
})

const upload = multer({ storage : storage });




router.get('/', function(req, res, next) {

	ProductModel.find()
	.then( product => {
		res.json(product)
	})
	.catch(next);
});

// single
router.get('/:id', (req, res, next) => {
	
	ProductModel.findOne({ _id : req.params.id})
	.then( product => res.json(product) )
	.catch(next)
});

// create
router.post('/', upload.single('image'),(req,res, next) => {
	// res.json(req.file)
	req.body.image = "/public/" + req.file.filename;
	ProductModel.create(req.body)
	.then( (product)=> res.json(product))
	.catch(next);
});

// update
router.put('/:id',
	passport.authenticate('jwt', { session: false }),
	isAdmin,
	upload.single('image'),
	(req,res,next) => {
		let update = {
			...req.body
		}
		if (req.file) {
			update = {
				...req.body,
				image: "/public/" + req.file.filename
			}
		}

		
		ProductModel.findByIdAndUpdate(req.params.id , update , { new:true })
		.then( product => res.json(product) )
		.catch(next);
	}
)

router.delete('/:id',(req,res,next) => {
	ProductModel.deleteOne({ _id : req.params.id })
	.then( product => res.json(product) )
	.catch(next)
})

module.exports = router;