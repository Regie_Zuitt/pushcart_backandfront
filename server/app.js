const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const passport = require('passport');



// set up server

// initialize the app
const app = express();

const port = 3001;

// connect to database
mongoose.connect(
	'mongodb://localhost/myapp', 
	{
		useUnifiedTopology: true,
		useNewUrlParser: true
	}
);
mongoose.connect('connected', () => {
	console.log("database connected")
})

// use dependencies/middleware
app.use(passport.initialize())
app.use(cors())
app.use('/public',express.static('public/products'));
app.use(bodyParser.json());



// app.use(myfunction);

// Routes
app.use('/categories', require('./routes/categories'));
app.use('/products',require('./routes/products'));
app.use('/users', require('./routes/users'));
app.use('/transactions', require('./routes/transactions'));

// error handling middleware
app.use(function(err,req,res,next){
	// console.log(err)
	res.status(400).json({
		error : err.message
	})
})

app.listen(port, () => {
	console.log(`Listening to port ${port}`)
})
